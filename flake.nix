{
  description = "NixOS configuration";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-24.05";
    home-manager.url = "github:nix-community/home-manager/release-24.05";
    home-manager.inputs.nixpkgs.follows = "nixpkgs";
    nixvim.url = "github:nix-community/nixvim/nixos-24.05";
    nixvim.inputs.nixpkgs.follows = "nixpkgs";
  };

  outputs = { self, nixpkgs, home-manager, nixvim }: {
    nixosConfigurations = {
      hedlund = nixpkgs.lib.nixosSystem {
        system = "x86_64-linux";
        modules = [
	        ./configuration.nix
	        nixvim.nixosModules.nixvim
	        home-manager.nixosModules.home-manager
	      ];
      };
    };
  };
}
