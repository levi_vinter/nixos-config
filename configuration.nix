# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, lib, pkgs, home-manager, ... }:

{
  imports =
    [ # Include the results of the hardware scan.
      ./hardware-configuration.nix
    ];

  # Bootloader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;

  networking.hostName = "nixos"; # Define your hostname.
  # networking.wireless.enable = true;  # Enables wireless support via wpa_supplicant.

  # Configure network proxy if necessary
  # networking.proxy.default = "http://user:password@proxy:port/";
  # networking.proxy.noProxy = "127.0.0.1,localhost,internal.domain";

  # Enable networking
  networking.networkmanager.enable = true;

  # Set your time zone.
  time.timeZone = "Europe/Stockholm";

  # Select internationalisation properties.
  i18n.defaultLocale = "en_US.UTF-8";

  i18n.extraLocaleSettings = {
    LC_ADDRESS = "sv_SE.UTF-8";
    LC_IDENTIFICATION = "sv_SE.UTF-8";
    LC_MEASUREMENT = "sv_SE.UTF-8";
    LC_MONETARY = "sv_SE.UTF-8";
    LC_NAME = "sv_SE.UTF-8";
    LC_NUMERIC = "sv_SE.UTF-8";
    LC_PAPER = "sv_SE.UTF-8";
    LC_TELEPHONE = "sv_SE.UTF-8";
    LC_TIME = "sv_SE.UTF-8";
  };

  # Enable the X11 windowing system.
  services.xserver.enable = true;

  # Enable the GNOME Desktop Environment.
  services.xserver.displayManager.gdm.enable = true;
  services.xserver.desktopManager.gnome.enable = true;

  # Configure keymap in X11
  services.xserver.xkb.layout = "se";
  services.xserver.xkb.variant = "";
  services.xserver.xkb.extraLayouts.en = {
    description = "Thomas' Colemak";
    languages   = [ "eng" ];
    symbolsFile = ./.xkb/symbols/colemak;
  };

  # Configure console keymap
  console.keyMap = "sv-latin1";

  # Enable CUPS to print documents.
  services.printing.enable = true;

  # Enable sound with pipewire.
  hardware.pulseaudio.enable = false;
  security.rtkit.enable = true;
  services.pipewire = {
    enable = true;
    alsa.enable = true;
    alsa.support32Bit = true;
    pulse.enable = true;
    # If you want to use JACK applications, uncomment this
    #jack.enable = true;

    # use the example session manager (no others are packaged yet so this is enabled by default,
    # no need to redefine it in your config for now)
    #media-session.enable = true;
  };

  # Enable touchpad support (enabled default in most desktopManager).
  # services.xserver.libinput.enable = true;

  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.users.hedlund = {
    isNormalUser = true;
    description = "Hedlund";
    extraGroups = [ "networkmanager" "wheel" "docker" ];
    shell = pkgs.zsh;
    packages = with pkgs; [
    #  thunderbird
    ];
  };

  # Install firefox.
  programs.firefox.enable = true;
  programs.zsh.enable = true;
  programs.nixvim = {
    enable = true;
    defaultEditor = true;
    enableMan = true;
    colorscheme = "nightfox";
    opts = {
      mouse = "a";
	  hidden = true;
	  cursorline = true;
	  expandtab = true;
      number = true;         # Show line numbers
      relativenumber = true; # Show relative line numbers
      shiftwidth = 4;        # Tab width should be 2
      tabstop = 4;
      clipboard = "unnamedplus";
      autoindent = true;
      smartindent = true;
      ignorecase = true;
      cindent = true;
      termguicolors = true;
    };
    globals.mapleader = " ";
    plugins = {
      lightline.enable = true;
    };
    extraPlugins = with pkgs.vimPlugins; [
      nightfox-nvim
    ];
	extraConfigVim = ''
      augroup nix_two_spaces
        autocmd!
        autocmd FileType nix setlocal shiftwidth=2
        autocmd FileType nix setlocal tabstop=2
      augroup END
    '';
  };

  # Allow unfree packages
  nixpkgs.config.allowUnfree = true;

  # List packages installed in system profile. To search, run:
  # $ nix search wget
  environment.systemPackages = with pkgs; [
     bitwarden-desktop
     wl-clipboard
     bat
     eza
     tldr
     zsh-powerlevel10k
     google-chrome
     jq
     dig
     wireshark
     whois
     openssl_3_3
     netcat
     unzip
  ];
  virtualisation.docker.enable = true;
  environment.pathsToLink = [ "/share/zsh" ];

  # Some programs need SUID wrappers, can be configured further or are
  # started in user sessions.
  # programs.mtr.enable = true;
  # programs.gnupg.agent = {
  #   enable = true;
  #   enableSSHSupport = true;
  # };

  # List services that you want to enable:

  # Enable the OpenSSH daemon.
  # services.openssh.enable = true;

  # Open ports in the firewall.
  # networking.firewall.allowedTCPPorts = [ ... ];
  # networking.firewall.allowedUDPPorts = [ ... ];
  # Or disable the firewall altogether.
  # networking.firewall.enable = false;

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "24.05"; # Did you read the comment?

  nix.settings.experimental-features = [ "nix-command flakes" ];

  home-manager.users.hedlund = { pkgs, ... }: {
    imports = [ ./dconf.nix ];
    home.packages = [ pkgs.htop ];
    programs.bash.enable = true;
    programs.git.enable = true;
    programs.git.ignores = [
      "_dev"
      ".python-version"
      "dev_azure_function"
      "__blobstorage__"
      "__azurite_"
      ".ideavimrc"
    ];
    programs.git.userEmail = "levivinter@protonmail.com";
    programs.git.userName = "Thomas Hedlund";
    programs.git.extraConfig = {
      core = {
        editor = "nvim";
      };
      pull = {
	ff = "only";
      };
      init = {
        defaultBranch = "main";
      };
      url = {
        "ssh://git@host" = {
          insteadOf = "otherhost";
        };
      };
    };
    programs.zsh.enable = true;
    programs.zsh.enableCompletion = true;
    programs.zsh.enableVteIntegration = true;
    programs.zsh.autocd = true;
    programs.zsh.autosuggestion.enable = true;
    programs.zsh.history.expireDuplicatesFirst = true;
    programs.zsh.history.ignoreAllDups = true;
    programs.zsh.history.ignoreDups = true;
    programs.zsh.history.save = 20000;
    programs.zsh.history.share = true;
    programs.zsh.history.size = 20000;
    programs.zsh.oh-my-zsh.enable = true;
    programs.zsh.oh-my-zsh.plugins = [
      "git"
      "z"
    ];
    programs.zsh.zsh-abbr.enable = true;
    programs.zsh.zsh-abbr.abbreviations = {
      dc = "docker compose";
      dce = "docker compose exec";
      dcu = "docker compose up";
      ll = "eza -lag";
      cat = "bat";
      gs = "git status";
      gc = "git commit -vs";
      gl = "git log --oneline --graph --decorate --date-order --all --date=format-local:'%Y-%m-%d %H:%M' --pretty=format:'%C(auto)%h%d %s %Cblue[%ad by %an]%Creset'";
      debugpy = "python -m debugpy --wait-for-client --listen 12345";
      azurefunc = "azurite --silent &; func host start --functions dev_azure_function --language-worker -- \"-m debugpy --listen 12345\"";
      awslogin = "saml2aws login --skip-verify --force && eval $(saml2aws script)";
      openfortigui = "sudo sh -c 'nohup /usr/bin/openfortigui > ~/nohup.out 2>&1 &'";
      tf = "terraform";
      fd = "fd -I -H";
    };
    programs.zsh.syntaxHighlighting.enable = true;
    programs.zsh.plugins = [
      {
        name = "powerlevel10k";
        src = pkgs.zsh-powerlevel10k;
        file = "share/zsh-powerlevel10k/powerlevel10k.zsh-theme";
      }
      {
        name = "powerlevel10k-config";
        src = ./p10k-config;
        file = "p10k.zsh";
      }
    ];
    programs.vscode.enable = true;
    programs.vscode.enableExtensionUpdateCheck = true;
    programs.vscode.extensions = with pkgs; [
      vscode-extensions.bbenoist.nix
      vscode-extensions.vscodevim.vim
      vscode-extensions.mhutchie.git-graph
      vscode-extensions.golang.go
    ];
    programs.kitty.enable = true;
    programs.kitty.keybindings = {
      "alt+l" = "next_window";
      "alt+h" = "previous_window";
      "alt+k" = "next_tab";
      "alt+j" = "previous_tab";
      "alt+shift+i" = "select_tab";
      "kitty_mod+w" = "close_window";
      "kitty_mod+l" = "next_layout";
      "ctrl+shift+z" = "toggle_layout stack";
      "kitty_mod+enter" = "launch --cwd=current";
    };
    programs.kitty.shellIntegration.enableZshIntegration = true;
    programs.kitty.shellIntegration.enableBashIntegration = true;
    programs.kitty.shellIntegration.mode = "no-cursor";
    programs.kitty.theme = "Nightfox";
    programs.kitty.settings = {
      scrollback_lines = 20000;
      enable_audio_bell = false;
      update_check_interval = 0;
      cursor_shape = "block";
      wayland_titlebar_color = "#1d202f";
      enabled_layouts = "tall,fat,stack";
      tab_bar_style = "fade";
      tab_fade = "1";
    };
    programs.ripgrep.enable = true;
    programs.ripgrep.arguments = [
      "--smart-case"
      "--type-add"
      "tpl:*.tpl"
      "--follow"
      "--no-ignore-vcs"
    ];
  
    # The state version is required and should stay at the version you
    # originally installed.
    home.stateVersion = "24.05";
  };

  home-manager.useUserPackages = true;
  home-manager.useGlobalPkgs = true;

}
