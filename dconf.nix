# Generated via dconf2nix: https://github.com/gvolpe/dconf2nix
{ lib, ... }:

with lib.hm.gvariant;

{
  dconf.settings = {
    "org/gnome/desktop/wm/keybindings" = {
      switch-to-workspace-1 = [ "<Super>7" ];
      switch-to-workspace-2 = [ "<Super>8" ];
      switch-to-workspace-3 = [ "<Super>9" ];
      switch-to-workspace-4 = [ "<Super>0" ];
    };
    "org/gnome/shell/keybindings" = {
      switch-to-application-7 = [];
      switch-to-application-8 = [];
      switch-to-application-9 = [];
    };
  };
}
